# Passenger on-board information

Ce side-project a pour objectif de simuler une application d'affichage d'information voyageur à bord d'un bus/tramway/métro/...

Le projet est divisé en deux parties :
- La partie "serveur", réalisée en NodeJS, permet la gestion des données à afficher. Elle intègre aussi un dashboard de pilotage des données à afficher.
- La partie "client", réalisée en Angular, gère l'affichage des informations à destination des voyageurs sur les écrans situés à bord du véhicule.

Le client et le serveur communiquent en temps réel grâce à la librairie [socket.io](https://socket.io/).

## Initialisation de l'application
Afin d'initialiser l'application, exécuter les commandes suivantes :
```bash
# Cloner le dépôt en local
git clone https://gitlab.com/vbochet/passenger-onboard-info.git

# Initialiser la partie serveur de l'application
cd passenger-onboard-info/server
npm install

# Initialiser la partie client de l'application
cd ../client
npm install
```

## Le serveur
### Exécution
Afin d'exécuter l'application serveur, exécuter la commande suivante dans le répertoire `server` :
```bash
node app.js
```

### Fonctionnement
L'application met à disposition un panneau de gestion permettant de sélectionner la ligne à afficher, sa destination, de gérer le passage des différentes stations et les messages d'information. Celui-ci est accessible à l'adresse http://localhost:3000.

### Données
Les données des lignes "connues" par l'application sont stockées dans le fichier `donneesLignes.js`. Ces données sont stockées au format JSON.

`stationsLigne` est un dictionnaire contenant pour chaque ligne l'ensemble des stations desservies et les correspondances.
- L'attribut `nom` correspond au nom de la station.
- L'attribut `direction` correspond aux directions dans lesquelles la station est desservie : `0` si la station est desservie dans les deux sens, `1` si la station est desservie dans le sens amont->aval uniquement et `-1` si la station est desservie dans le sens aval->amont uniquement.
- L'attribut `correspondances` correspond aux noms des lignes en correspondance, séparées en trois groupes maximum (ces groupes seront affichés sur des lignes distinctes sur le client d'affichage).

Exemple :
```
{
    "nom":"Boulingrin",
    "direction":"0",
    "correspondances":[["T4"],["F2"],["22","40"]]
}
```

`couleurLigne` est un dictionnaire contenant le code couleur de chaque ligne en hexadécimal.

*NB : Les données d'exemple fournies dans l'application sont issues du réseau de transports en commun de l'agglomération de Rouen, le [réseau Astuce](http://reseau-astuce.fr).*

## Le client
### Exécution
Afin d'exécuter l'application client, exécuter la commande suivante dans le répertoire `client` :
```bash
npm run start
```

### Configuration
Pour s'exécuter correctement, l'application client a besoin de se connecter au serveur. L'URL de connexion se situe dans le fichier `proxy.conf.json`, à la racine du projet client. La valeur par défaut est `http://localhost:3000`.

### Fonctionnement
Au démarrage, l'application affiche un écran de maintenance en attendant d'établir une connexion au serveur et d'en obtenir les informations de la ligne à afficher.

Lorsqu'une ligne est sélectionnée et qu'un horaire de départ est planifié, l'affichage est remplacé par un compte à rebours jusqu'à atteindre l'heure dite.

Une fois l'heure de départ passée, l'affichage peut se trouver dans deux modes distincts :
- on se trouve à l'arrêt à une station : dans ce cas, l'affichage indique le nom de la station ainsi que les lignes en correspondance ;
- on se trouve en déplacement entre deux stations : dans ce cas, l'affichage alterne toutes les 15 secondes entre la nom de la prochaine station (accompagné des correspondances) et un mini-plan indiquant les 5 prochaines stations.

Lorsqu'on atteint le terminus, l'affichage alterne entre le nom de la station et la mention "terminus".