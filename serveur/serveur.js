const express = require('express');

var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var adminConnections = [];

app.use('/css', express.static(__dirname + '/dashboard/css'));
app.use('/js', express.static(__dirname + '/dashboard/js'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/dashboard.html');
});

let donnees = require('./donneesLignes');
var stationsLigne = donnees.stationsLigne;
var couleurLigne = donnees.couleurLigne;

var data = {
    ligne: undefined,
    messagesInfo: []
};

var nextMessageId = 1;

io.on('connection', (socket) => {
    adminConnections.push(socket);
    console.log(adminConnections.length + ' client(s) connecté(s)');

    io.emit('init', data);

    socket.on('disconnect', () => {
        adminConnections.splice(adminConnections.indexOf(socket), 1);
        console.log('Un client déconnecté');
        if (adminConnections.length <= 0) {
            console.log('Aucun client restant');
        } else {
            console.log(adminConnections.length + ' client(s) restant(s)');
        }
    });

    socket.on('maintenance_all_clients', (activer) => {
        io.emit('maintenance', activer);
        data.maintenance = activer;
    });

    socket.on('ligne', (ligne) => {
        if (stationsLigne[ligne.indice]) {
            ligne.stations = stationsLigne[ligne.indice].filter(station => station.direction >= 0);
            ligne.idStation = 0;
            if (!ligne.destination || ligne.destination.length === 0) {
                let indiceDestination = (ligne.direction == 1 ? ligne.stations.length - 1 : 0);
                ligne.destination = ligne.stations[indiceDestination].nom;
            }
        } else {
            ligne.stations = [];
            ligne.idStation = undefined;
        }

        if (couleurLigne[ligne.indice]) {
            ligne.couleur = couleurLigne[ligne.indice];
        } else {
            ligne.couleur = 'black';
        }

        ligne.horaireDepart = 0;
        ligne.position = 'arret';
        io.emit('ligne', ligne);
        data.ligne = ligne;
    });

    socket.on('direction', (direction) => {
        if (direction > 0) {
            data.ligne.stations = stationsLigne[data.ligne.indice].filter(station => station.direction >= 0);
            data.ligne.destination = data.ligne.stations[data.ligne.stations.length - 1].nom;
        } else {
            data.ligne.stations = stationsLigne[data.ligne.indice].filter(station => station.direction <= 0);
            data.ligne.destination = data.ligne.stations[0].nom;
        }
        data.ligne.direction = direction;
        data.ligne.position === 'arret';
        io.emit('ligne', data.ligne);
    });

    socket.on('horaireDepart', (timestamp) => {
        data.ligne.horaireDepart = timestamp;
        io.emit('ligne', data.ligne);
    });

    socket.on('incrIdStation', () => {
        if (data.ligne.idStation < data.ligne.stations.length - 1 && data.ligne.position === 'arret') {
            data.ligne.idStation++;
        }
        // TODO soit terminus et on peut boucler infiniment déplacement/arrêt
        //      soit on passe directement de arrêt terminus-1 à arrêt terminus sans la phase déplacement vers terminus
        if (data.ligne.idStation <= data.ligne.stations.length - 1 || data.ligne.position !== 'arret') {
            data.ligne.position = (data.ligne.position === 'arret' ? 'deplacement' : 'arret');
            if (data.ligne.position === 'deplacement') {
                data.ligne.timerReference = new Date().valueOf();
            }
        }
        io.emit('ligne', data.ligne);
    });
    socket.on('resetIdStation', () => {
        data.ligne.idStation = 0;
        data.ligne.position === 'arret';
        io.emit('ligne', data.ligne);
    });

    socket.on('msgInfoAdd', (message) => {
        data.messagesInfo.push({id: (nextMessageId++), message: message});
        io.emit('msgInfoList', data.messagesInfo);
    });
    socket.on('msgInfoDelete', (idMessage) => {
        data.messagesInfo = data.messagesInfo.filter(message => !(message.id === idMessage));
        io.emit('msgInfoList', data.messagesInfo);
    });
});

http.listen(3000, () => {
    console.log('Interface d\'administration à l\'écoute sur http://localhost:3000');
});
