import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ecran',
  templateUrl: './ecran.component.html',
  styleUrls: ['./ecran.component.css']
})
export class EcranComponent implements OnInit {

  @Input()
  public ligne: any;
  @Input()
  public messagesInfo: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  getStation(): any {
    if (!this.ligne.stations) {
      return undefined;
    }
    if (this.ligne.direction == 1) {
      return this.ligne.stations[this.ligne.idStation];
    } else {
      let idStation = this.ligne.stations.length - 1 - this.ligne.idStation;
      return this.ligne.stations[idStation];
    }
  }

  getNextStations() {
    if (!this.ligne.stations) {
      return [];
    }
    let nextStations = this.ligne.stations;
    if (this.ligne.direction == -1) {
      nextStations = this.ligne.stations.reverse();
    }
    return nextStations.slice(this.ligne.idStation-1, this.ligne.idStation+5);
  }

  isStarted(): boolean {
    return this.ligne.horaireDepart && this.ligne.horaireDepart !== 0
           && 0 >= Math.ceil((this.ligne.horaireDepart - (new Date().valueOf())) / 60000);
  }

  getCurrentDisplay(): any {
    if (!this.isStarted()) {
      return 'minuterie';
    } else {
      if (this.ligne.stations) {
        let diff = Math.ceil((new Date().valueOf() - this.ligne.timerReference) / 1000);
        if ((diff/15)%2 >= 1 || this.ligne.idStation === 0) {
          return 'station';
        } else {
          if (this.ligne.idStation === this.ligne.stations.length - 1 && this.ligne.position === 'arret') {
            return 'terminus';
          } else if (this.ligne.idStation < this.ligne.stations.length - 1 && this.ligne.position === 'arret') {
            return 'station';
          } else {
            return 'plan';
          }
        }
      } else {
        return '';
      }
    }
  }
}
