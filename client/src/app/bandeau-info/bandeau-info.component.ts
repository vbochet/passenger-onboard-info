import {  Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-bandeau-info',
  templateUrl: './bandeau-info.component.html',
  styleUrls: ['./bandeau-info.component.css']
})
export class BandeauInfoComponent implements OnInit {

  @Input()
  public messages: any = [];
  public currentMessage: any = { message: "", duree: 0 };

  private index: number = 0;
  private refTimestamp: number = 0;
  private isInitialized: boolean = false;
  
  @ViewChild('currentMessageElement')
  private currentMessageElement: ElementRef;

  constructor() { }

  ngOnInit(): void {
    this.refTimestamp = new Date().getTime() / 1000;
    setInterval(() => {
      if (!this.isInitialized && this.messages.length > 0) {
        this.isInitialized = true;
        this.index = 0;
        this.currentMessage.message = this.messages[this.index].message;
        this.currentMessage.duree = 10;
        this.currentMessage.style = undefined;
        this.currentMessage.wait = 2;
        this.refTimestamp = new Date().getTime() / 1000;
        return;
      }

      if (this.messages.length === 0) {
        this.isInitialized = false;
        return;
      }

      let nombreDeSecondesEcoulees = Math.floor((new Date().getTime() / 1000) - this.refTimestamp);

      if (nombreDeSecondesEcoulees > this.currentMessage.duree) {
        this.index = (this.index + 1) % this.messages.length;
        this.currentMessage.message = this.messages[this.index].message;
        this.currentMessage.style = undefined;
        this.currentMessage.wait = 2;
        this.refTimestamp = new Date().getTime() / 1000;
      }

      if (this.currentMessage.wait > 0) {
        this.currentMessage.wait--;
        this.currentMessage.duree = this.currentMessageElement.nativeElement.offsetWidth / 70;
        if (this.currentMessage.wait === 0) {
          this.currentMessage.style = { 'animation': 'scroll-left ' + this.currentMessage.duree +'s linear 1' };
        }
      }
    }, 500);
  }
}
