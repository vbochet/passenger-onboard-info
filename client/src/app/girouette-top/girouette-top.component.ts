import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-girouette-top',
  templateUrl: './girouette-top.component.html',
  styleUrls: ['./girouette-top.component.css']
})
export class GirouetteTopComponent implements OnInit {

  @Input()
  public destination: String;

  @Input()
  public indice: String;

  constructor() { }

  ngOnInit(): void { }

}
