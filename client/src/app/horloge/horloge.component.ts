import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-horloge',
  templateUrl: './horloge.component.html',
  styleUrls: ['./horloge.component.css']
})
export class HorlogeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  getHeure() {
    return new Date().toLocaleTimeString().substring(0,5);
  }
}
