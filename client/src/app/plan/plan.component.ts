import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {
  
  @Input()
  public stations: any;
  @Input()
  public couleur: any;

  constructor() { }

  ngOnInit(): void { }

  getClassLigne(): String {
    let nb = Math.min(this.stations.length - 1, 5);
    // width: 11 + (nb - 1) * 45;
    return 'w-' + nb;
  }
}
