import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-minuterie',
  templateUrl: './minuterie.component.html',
  styleUrls: ['./minuterie.component.css']
})
export class MinuterieComponent implements OnInit {

  @Input()
  public horaireDepart: number;

  constructor() { }

  ngOnInit(): void { }

  getAttente(): number {
    return Math.round((this.horaireDepart - (new Date().valueOf())) / 60000);
  }
}
