import { Component } from '@angular/core';
import { io, Socket } from 'socket.io-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  public isMaintenance: boolean = true;
  public ligne: any = {
    indice: '',
    destination: '',
    horaireDepart: 0,
    idStation: 0,
    stations: [],
    couleur: 'black'
  }
  public messagesInfo = [];
  private socket: Socket;

  constructor() {
    // Connect Socket with server URL
    this.socket = io(); // L'URL du serveur est à configurer dans le fichier proxy.conf.json
  }

  public ngOnInit(): void {
    this.socket.on('init', (data: any) => {
      this.isMaintenance = data.maintenance;
      this.ligne = data.ligne;
      this.messagesInfo = data.messagesInfo;
    });

    this.socket.on('maintenance', (activer: boolean) => {
      this.isMaintenance = activer;
    });

    this.socket.on('ligne', (ligne: any) => {
      this.ligne = ligne;
    });

    this.socket.on('msgInfoList', msgList => {
      this.messagesInfo = msgList;
    });
  }
}
