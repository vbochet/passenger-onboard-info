import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import { EcranComponent } from './ecran/ecran.component';
import { HorlogeComponent } from './horloge/horloge.component';
import { GirouetteTopComponent } from './girouette-top/girouette-top.component';
import { BandeauInfoComponent } from './bandeau-info/bandeau-info.component';
import { StationComponent } from './station/station.component';
import { MinuterieComponent } from './minuterie/minuterie.component';
import { PlanComponent } from './plan/plan.component';
import { TerminusComponent } from './terminus/terminus.component';

@NgModule({
  declarations: [
    AppComponent,
    MaintenanceComponent,
    EcranComponent,
    HorlogeComponent,
    GirouetteTopComponent,
    BandeauInfoComponent,
    StationComponent,
    MinuterieComponent,
    PlanComponent,
    TerminusComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
