import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css']
})
export class StationComponent implements OnInit {

  @Input()
  public station: any;
  @Input()
  public showCorrespondances: boolean;

  constructor() { }

  ngOnInit(): void { }

  calculerAffichage(nomStation: String): String {
    if (nomStation.length < 30) {
      return nomStation;
    }

    let ligne_1 = '';
    let ligne_2 = '';
    
    if (nomStation.includes(" - ")) {
        const text_split = nomStation.split(" - ");
        ligne_1 = text_split[0];
        ligne_2 = text_split[1];
    } else {
        const text_split = nomStation.split(" ");

        ligne_1 = text_split[0];
        let i = 1;
        while (ligne_1.length + text_split[i].length < 29) {
          ligne_1 += " " + text_split[i];
          i++;
        }

        ligne_2 = text_split[i];
        i++;
        while (i < text_split.length) {
          ligne_2 += " " + text_split[i];
          i++;
        }
    }

    return ligne_1 + '\n' + ligne_2;
  }

  private indiceComparator(a: string, b: string): number {
    let aNum = parseInt(a);
    let bNum = parseInt(b);

    if (isNaN(aNum) && isNaN(bNum)) {
      return a.localeCompare(b);
    } else if (isNaN(aNum) && !isNaN(bNum)) {
      return 1;
    } else if (!isNaN(aNum) && isNaN(bNum)) {
      return -1;
    } else {
      return aNum < bNum ? -1 : 1;
    }
  }

  trierIndices(correspondances: any): any {
    correspondances.sort(this.indiceComparator);
    return correspondances;
  }
}
